#!/usr/bin/python

from urllib import urlencode
import urllib2
import time
from hashlib import sha512 
from hmac import HMAC
import json
import base64

import exchange
import requester
import keys

class ReqBtc24(requester.Requester):
    def __init__(self):
        self.req_hist = {}
        self.limit = 5
        self.user = keys.getBtc24User()
        self.auth_key = keys.getBtc24Key()

    def buildQuery(self, req={}):
        post_data = urlencode(req)
        headers = {}
        return (post_data, headers)

    def buyBtc(self, amount, price):
        return self.placeOrder('buy_btc', amount, price)

    def cancelOrder(self, order_id):
        params = {'id': order_id}
        response = self.perform(params, 'cancel_order')
        return response['message']

    def sellBtc(self, amount, price):
        return self.placeOrder('sell_btc', amount, price)

    def getMyAddress(self):
        return self.perform({}, 'get_addr')

    def getBalance(self):
        return self.perform({}, 'get_balance')

    def getBalanceUsd(self):
        return self.getBalance()['usd']
    
    def getBalanceBtc(self):
        return self.getBalance()['btc']

    def sendBtc(self, amount, destination):
        params = {'amount': amount, 'address': destination}
        order_num = self.perform(params, 'withdraw_btc')['trans']
        return "The transaction number is %s" % order_num

    def listOpenOrders(self):
        return self.perform({}, 'open_orders')

    def listTrades(self):
        return self.perform({}, 'trades_json')

    def placeOrder(self, kind, amount, price):
        if kind not in ('buy_btc', 'sell_btc'):
            raise ValueError('kind must be one of buy_btc, sell_btc')
        params = {'amount': amount, 'price': price, 'cur': 'USD'}
        return(self.perform(params, kind))

    def perform(self, args, qtype):
        if qtype in self.req_hist and time.time() - self.req_hist[qtype][0] < self.limit:
            return self.req_hist[qtype][1]
        else:
            self.req_hist[qtype] = (time.time(), {})
        
        args['user'] = self.user
        args['key'] = self.auth_key
        args['api'] = qtype

        data, headers = self.buildQuery(args)
        req = urllib2.Request('https://bitcoin-24.com/api/user_api.php', data, headers)
        res = urllib2.urlopen(req, data)
        result = json.load(res)
        self.req_hist[qtype] = (self.req_hist[qtype][0], result)
        return result


if __name__ == '__main__':
    r = ReqBtc24()
    print 'BTC: ', r.getBalanceBtc()
    print 'USD: ', r.getBalanceUsd()

