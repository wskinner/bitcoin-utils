def getMtGoxKey():
    return '<your mtgox api key>'

def getMtGoxSecret():
    return '<your mtgox secret>'

def getBtc24Key():
    return '<your btc24 api key>'

def getBtc24User():
    return '<your btc24 username>'
