#!/usr/bin/python

from urllib import urlencode
import urllib2
import time
from hashlib import sha512 
from hmac import HMAC
import json
import base64

import exchange
import requester
import keys


def getNonce():
    return int(time.time()*100000)

def signData(secret, data):
    return base64.b64encode(str(HMAC(secret, data, sha512).digest()))

def getAmountInt(amount, kind):
    if kind == 'btc':
        return int(amount*10**8)
    elif kind == 'usd':
        return int(amount*10**5)
    else:
       raise ValueError('Argument must be one of (usd, btc)')


class ExMtgox(exchange.Exchange):
    def __init__(self):
        super(Exchange, self).__init__()


class ReqMtgox(requester.Requester):
    def __init__(self, auth_key=keys.getMtGoxKey(),
                auth_secret=keys.getMtGoxSecret()):
        self.auth_key = auth_key
        self.auth_secret = base64.b64decode(auth_secret)

        self.req_hist = {}

    def getCurrentPrice(self):
        return

    def buyBtc(self, amount, price):
        return self.placeOrder(amount, 'bid', price)

    def getBalanceBtc(self):
        balance = self.getPrivateInfo()['return']['Wallets']['BTC']['Balance']['value']
        return balance

    def getBalanceUsd(self):
        balance = self.getPrivateInfo()['return']['Wallets']['USD']['Balance']['value']
        return balance

    def getCurrentPrice(self):
        last_price = self.getTicker()['return']['last']['value']
        return last_price

    def getDepth(self):
        path = 'BTCUSD/depth/fetch'
        return self.perform(path, {})
        
    def getHigh(self):
        return 

    def getLow(self):
        return

    def getPrivateInfo(self):
        path = 'generic/private/info'
        return self.perform(path, {})

    def getTicker(self):
        path = 'BTCUSD/ticker'
        return self.perform(path, {})

    def sellBtc(self, amount, price_per_coin):
        return self.placeOrder(amount, 'ask', price)

    def sendBtc(self, amount, destination, fee_int=None, no_instant=None, green=None):
        params = {'address': destination, 'amount_int': getAmountInt(amount, 'btc')}
        if fee_int:
            params['fee_int'] = fee_int
        if no_instant:
            params['no_instant'] = no_instant
        if green:
            params['green'] = green

        path = 'generic/bitcoin/send_simple'
        return self.perform(path, params)

    def placeOrder(self, amount, kind, price):
        if kind not in ('ask', 'bid'):
            raise ValueError('kind must be one of (ask, bid)')

        amount_int = getAmountInt(amount, 'btc')
        price_int = getAmountInt(price, 'usd')

        path = 'BTCUSD/private/order/add'
        params = {'type': kind, 'amount_int': amount_int, 'price_int': price_int}
        return self.perform(path, params)


    def buildQuery(self, req={}):
        req['nonce'] = getNonce()
        post_data = urlencode(req)
        headers = {}
        headers['User-Agent'] = 'GoxApi'
        headers['Rest-Key'] = self.auth_key
        headers['Rest-Sign'] = signData(self.auth_secret, post_data)
        return (post_data, headers)

    def perform(self, path, args, api_version=1):
        if path in self.req_hist and time.time() - self.req_hist[path][0] < 10:
            return self.req_hist[path][1]
        else:
            self.req_hist[path] = (time.time(), {})

        data, headers = self.buildQuery(args)
        req = urllib2.Request('https://data.mtgox.com/api/%s/%s' % (api_version, path), data, headers)
        res = urllib2.urlopen(req, data)
        result = json.load(res)
        self.req_hist[path] = (self.req_hist[path][0], result)
        return result

if __name__ == '__main__':
    rmg = ReqMtgox()
    dst = '1PAoSdVYRNUxi5bESfsF8vMmxb3bS69YiP'
    #print rmg.sendBtc(0.2, dst)

    #print rmg.buyBtc(0.2, 65.0)

    print 'BTC: ', rmg.getBalanceBtc()
    print 'USD: ', rmg.getBalanceUsd()
