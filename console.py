#!/usr/bin/python

import re
import functools

from Levenshtein import jaro_winkler as dist

from src.btc24 import ReqBtc24 as Btc24
from src.mtgox import ReqMtgox as Mtgox


class BtcConsole:
    exchange_map = {'btc24': Btc24, 'mtgox': Mtgox}

    def __init__(self, exchanges):
        self.exchanges = {}
        for e in exchanges:
            if e in BtcConsole.exchange_map:
                self.exchanges[e] = BtcConsole.exchange_map[e]()

        self.commands = {}
        for name,ex in self.exchanges.items():
            self.commands[name] = [c for c in dir(ex) if c[0]!='_']

    def execute(self, qlist):
        if qlist[1] == 'help':
            print 'Commands for', qlist[0]+':'
            for c in self.commands[qlist[0]]:
                print '*', c
            return
        exchange  = self.exchanges[qlist.pop(0)]
        func = getattr(exchange, qlist.pop(0))
        name = str(func).split()[2].split('.')[-1]
        print '***Executing', name, str(qlist)
        response = func(*qlist)
        self.handleOutput(response)

    def cleanInput(self, ls):
        if ls[1] == 'help':
            return ls
        cmd = ls[1]
        best = ('', -float('inf'))
        for c in self.commands[ls[0]]:
            d = dist(c.lower(), cmd)
            if d > best[1]:
                best = c, d
        ls[1] = best[0]
        return ls

    def run(self):
        while True:
            cmd = raw_input('<Trader>$ ')
            if cmd == 'exit':
                break
            else:
                cmdlist = cmd.strip().split()
                if len(cmdlist) < 2:
                    print 'Format: <exchange> command [args]'
                else:
                    try:
                        self.execute(self.cleanInput(cmdlist))
                    except:
                        print 'Command failed for some reason...'

    def handleOutput(self, out):
        print out

if __name__ == '__main__':
    from sys import argv
    if len(argv) > 1:
        con = BtcConsole(argv[1:])
        con.run()
    else:
        print 'You must choose at least one exchange:'
        print './console ex1, [ex2...]'


            

dist
