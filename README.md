# What is it?
This package provides some utilities for interacting with bitcoin exchanges like
Mt. Gox. An abstraction layer is implemented on top of the individual exchange
APIs. 

# Installation
```bash
git clone git@github.com:wskinner/bitcoin-utils.git
cd bitcoin-utils
pip install -r requirements.txt
```

# Usage
Before running the program, you will need to add your API keys to a file called
src/keys.py. For each exchange you want to use, fill out the corresponding 
section in src/keys.py.

After adding your keys, you can interact with your exchange accounts by running
```bash
./console <exchange1> <exchange2> ... 
```
The general format is 
```bash
<exchange> cmd [args].
```
Run e.g.
```bash
<mtgox> help
``` 
To list the available commands for Mt. Gox.

# Warning
This software is currently under development... it probably shouldn't even be
considered alpha. Use at your own risk.
